/**
 * 
 */

/**
 * @author Corey
 *
 */
public class Practice {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		BinaryTree tree = new BinaryTree();
		
		//node insert block
		tree.insertNode(50, "cow");
		tree.insertNode(3, "duck");
		tree.insertNode(55, "chicken");
		tree.insertNode(26, "pig");
		tree.insertNode(87, "donkey");
		tree.insertNode(64, "sheep");
		tree.insertNode(2, "ostrich");
		tree.insertNode(90, "rabbit");
		tree.insertNode(54, "egret");
		tree.insertNode(25, "bull");
		
		//display tree with in order traversal (smallest to largest key)
		//tree.inOrderTraversal(tree.Root);
		
		//display tree with pre order traversal (root, then left to right)
		//tree.preOrderTraversal(tree.Root);
		
		//display tree with post order traversal (Need to learn how to describe it better - right to left nodes of subtrees?)
		//tree.postOrderTraversal(tree.Root);
		
		//display tree in order from root to bottom by level from left to right
		//tree.toStringByLevel(tree.Root);
		
		//display tree in order from root to bottom by level from left to right in a more sensible manner
		tree.levelOrderTraversal();

	}

}
