/**
 * 
 */

/**
 * @author Corey
 *	
 *	Node class is individual nodes of tree. Each node has at most one right child and
 *		one left child. Can have neither. 
 */

//Node class is individual nodes of tree. Each node has at most one right child and
//one left child. Can have neither. 

public class Node {
	
	int key;
	String content;
	
	Node LeftChild;
	Node RightChild;
	
	Node(int key, String content){
		this.key = key;
		this.content = content;
	}
	
	//to string method returns string of current node information
	public String toString(){
		return "Node " + this.key + " has value " + this.content + ".";
	}

}