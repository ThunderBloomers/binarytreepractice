/**
 * 
 */

/**
 * @author Corey
 *
 *	Tree structure. On insert, will check values and children to determine placement
 */

import java.util.Queue;
import java.util.LinkedList;

public class BinaryTree {
	
	Node Root;
	
	//empty constructor could be left out
	BinaryTree(){}


	public void insertNode(int key, String content) {
		Node newNode = new Node(key, content);
				
		//if a root node exists
		if(this.Root != null){
			
			Node currentNode = this.Root;
			Node parentNode;
			
			//infinite loop
			while(true){
				
				//update focus to parent node
				parentNode = currentNode;
				
				//if the key should be on the left of the current node
				if (key < currentNode.key){
					
					//change current to the left child of current node
					currentNode = parentNode.LeftChild;
					
					//if left child is null, make newNode new left child
					if(currentNode == null){
						parentNode.LeftChild = newNode;
						System.out.println("Inserting " + newNode.key + " left of " + parentNode.key + "\n");
						return;
					}
					
				//key should be on right of current node (duplicates will go here)
				}else{
					
					//change current to the right child of current node
					currentNode = parentNode.RightChild;
					
					//if right child is null, make newNode new right child
					if(currentNode == null){
						parentNode.RightChild = newNode;
						System.out.println("Inserting " + newNode.key + " right of " + parentNode.key + "\n" );
						return;
					}
				}
			}
			
		//no root exists, create new one
		}else{
			this.Root = newNode;
			System.out.println("\nInserting " + newNode.key + " as root" + "\n");
		}

	}
	
	public void inOrderTraversal(Node currentNode){
		if (currentNode != null){
			
			inOrderTraversal(currentNode.LeftChild);
			
			System.out.println(currentNode);
			
			inOrderTraversal(currentNode.RightChild);
		}
		
	}
	
	public void preOrderTraversal(Node currentNode){
		if (currentNode != null){
			
			System.out.println(currentNode);
			
			preOrderTraversal(currentNode.LeftChild);
			preOrderTraversal(currentNode.RightChild);
		}
	}
	
	public void postOrderTraversal(Node currentNode){
		if (currentNode != null){
			
			postOrderTraversal(currentNode.LeftChild);
			postOrderTraversal(currentNode.RightChild);
			
			System.out.println(currentNode);
		}
	}
	
	//prints binary tree starting at root, then in left to right order of children by level using some complicated stupid function
	public void toStringByLevel(Node currentNode){

			if(currentNode!=null){
				System.out.println(currentNode);
			}
			
			doubleCurse(currentNode.LeftChild, currentNode.RightChild);
	}
	
	//Does not work. Poor attempt. Bad code. Shame.
	private void doubleCurse(Node left, Node right){
			
			if(left != null && right != null){
				
				System.out.println(left);
				System.out.println(right);
				
				doubleCurse(left.LeftChild, left.RightChild);
				doubleCurse(right.LeftChild, right.RightChild);
				
			}else if(left != null){
				System.out.println(left);
				doubleCurse(left.LeftChild, left.RightChild);
				
			}else if(right != null){
				System.out.println(right);
				doubleCurse(right.LeftChild, right.RightChild);
			}
		
	}
	
	//this is better. less shame.
	public void levelOrderTraversal(){
		
		Queue<Node> tempFlatTree = new LinkedList<Node>();
		tempFlatTree.add(this.Root);
		
		Node currentNode;
		
		//while there are still nodes in the queue
		while(!tempFlatTree.isEmpty()){
			
			//pull the current head of the queue
			currentNode = tempFlatTree.poll();
			
			//display current head of  queue
			System.out.println(currentNode);
			
			//store children of current node
			if(currentNode.LeftChild != null){
				tempFlatTree.add(currentNode.LeftChild);
			}
			
			if(currentNode.RightChild != null){
				tempFlatTree.add(currentNode.RightChild);
			}
		}
		
	}
	
	
}
